from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='mitgcmdata',
      version='0.1',
      description='Python tools for analyzing MITgcm output',
      url='https://bitbucket.org/ryanaberanthey/mitgcmdata',
      author='Ryan Abernathey',
      author_email='rpa@ldeo.columbia.edu',
      license='MIT',
      packages=['mitgcmdata'],
      install_requires=[
          'MITgcmutils',
      ],
      zip_safe=False)
