# used to be part of ModelInstance
    # special functions
    # def get_layers_computer(self, **kwargs):
    #     try:
    #         layers_G = mds.rdmds( os.path.join(self.grid_dir, 'layers_G') )
    #     except IOError:
    #         layers_G = None
    #     if layers_G is not None:
    #         kwargs['layers_G'] = layers_G
    #     return LayersComputer(self, **kwargs)
    #
    #
    # def get_APEcomputer(self, **kwargs):
    #     return APEcomputer(self, **kwargs)


class VTKoutput:
    # VTK output
    def output_VTK(self, fname, data, hpoint='C', vpoint='C', zscale=300):
        from evtk.hl import gridToVTK, pointsToVTK
        if hpoint=='C':
            x = self.xc
            y = self.yc
        elif hpoint=='S':
            x = self.xc
            y = self.xg
        elif hpoint=='W':
            x = self.xg
            y = self.yg
        elif hpoint=='Z':
            x = self.xg
            y = self.yg
        else:
            raise KeyError('Invalid hpoint specified')
        
        if vpoint=='C':
            z = self.zc
        elif vpoint=='F':
            z = self.zf
        else:
            raise KeyError('Invalid vpoint specified')
        
        if not isinstance(data, dict):
            data = {'data': data}
        
        for k in data.keys():
            if isinstance(data[k], ma.masked_array):
                data[k] = data[k].data.transpose()
            else:
                data[k]  = data[k].transpose()            
                   
        gridToVTK(fname, x.copy(), y.copy(), zscale*z.squeeze(),
                pointData=data)


class APEcomputer:
    
    def __init__(self, model, b=None, T_fname='Ttave', S_fname='Stave', iter=None):
        self.__dict__.update(locals())
        self.vol = (self.model.dzf[:,np.newaxis,np.newaxis] * self.model.rac[np.newaxis,:,:] * self.model.hFacC)
        # this definies the volume of the z grid
        # (indez of z goes down from the surface)
        self.zf_vol = hstack([0,cumsum(self.vol.sum(axis=2).sum(axis=1))])
        
        if b is None:
            T = self.model.rdmds(T_fname, itrs=iter)
            try:
                S = self.model.rdmds(S_fname, itrs=iter)
            except IOError:
                S = None
            self.b = self.model.get_buoyancy(T,S)
        
        self.compute_Zstar()
    
    def new_b(self,b):
        self.b = b
        self.compute_Zstar()
        

    
    def calc_PE(self):
        return sum( -self.b * self.model.zc[:,np.newaxis,np.newaxis] * self.vol )

    def calc_BPE(self):
        return sum( -self.Bstar[:,np.newaxis,np.newaxis] *
                    self.model.zc[:,np.newaxis,np.newaxis] * self.vol )
                        
    def calc_BPE_via_Zstar(self):
        return sum(self.vol * self.Zstar * -c.b)
    
    def calc_APE(self):
        return sum( (-self.b + self.Bstar[:,np.newaxis,np.newaxis])*
                    self.model.zc[:,np.newaxis,np.newaxis] * self.vol )    
    
    def Zstar_of_b(self,b):
        """Takes b as an input, returns the corresponding Z*"""
        return ma.masked_array(interp(-b,-self.Bstar_f,self.model.zf),b.mask)
    
    def calc_Bstar_f(self,b,vol,zf_vol):
        maskidx = find(vol.ravel()>0)
        b_flat = b.ravel()[maskidx]
        vol_flat = vol.ravel()[maskidx]
        sortidx = argsort(-b_flat)
        cumvol = cumsum(vol_flat[sortidx])
        return interp(zf_vol, cumvol, b_flat[sortidx])
    
    def compute_Zstar(self):
        """Creates the Zstar profile through sorting"""
        self.Bstar_f = self.calc_Bstar_f(self.b,self.vol,self.zf_vol)
        self.Bstar = 0.5 * (self.Bstar_f[1:] + self.Bstar_f[:-1])
        self.Zstar = self.Zstar_of_b(self.b)   

class LayersComputer:

    def __init__(self, model, layer_nb=1,
                 eos_type='linear', layers_G=arange(-1,26)+0.5,
                 u_fname='Uveltave', v_fname='Vveltave',
                 T_fname='Ttave', S_fname='Stave', fine_grid_fac=10
                ):
        """A class to facilitate computation of layer transport."""
        # automatically make set instance variables from arguments
        self.__dict__.update(locals())

        Nz = model.Nz
        # set up global vars for interpolation
        # (copy this from MITgcm LAYERS pacakge)
        self.Ng = len(self.layers_G)-1
        self.G = 0.5 * ( self.layers_G[:-1] + self.layers_G[1:] )
        self.Nzz = Nz * self.fine_grid_fac
        self.dZZf = empty(self.Nzz)
        self.map_fac = empty(self.Nzz)
        self.map_idx = empty(self.Nzz)
        # for quickly breaking down z-grid
        self.break_idx = tile(arange(Nz),[self.fine_grid_fac,1]).T.flatten()

        self.dZZf = self._break_into_zz(model.dzf)/self.fine_grid_fac
        #for k in arange(Nz):
        #   self.dZZf[ (k*self.fine_grid_fac):((k+1)*self.fine_grid_fac) ] = model.dzf[k]/self.fine_grid_fac
        self.ZZf = empty(self.Nzz+1)
        self.ZZf[1:] = -cumsum(self.dZZf)
        self.ZZc = 0.5 * ( self.ZZf[:-1] + self.ZZf[1:] )

        # It would probably be possible to use a built-in scipy interpolation routine,
        # but instead I have tried to follow the spirit of the layers code as closely as possible.
        raw_map_idx = interp(-self.ZZc, -model.zc, arange(Nz))
        self.map_idx = floor(raw_map_idx).astype('int')
        self.map_idx[self.map_idx==Nz-1] = Nz-2
        self.map_fac = 1 - (raw_map_idx - self.map_idx)

        # figure out if we can use the fortran module for speedy interpolation
        try:
            import fast_layers
            self.use_fortran = True
        except ImportError as ie:
            print ie
            warnings.warn('Unable to import fast_layers FORTRAN module. Interpolation will be via python (slow!)')
            self.use_fortran = False
    
    def transform_g_to_z(self, f, h, zpoint='C'):
        if f.shape != h.shape:
            raise ValueError('f and h must have the same shape')
        if zpoint=='C':
            z = self.model.zc
        elif zpoint=='F':
            z = self.model.zf
        else:
            raise ValueError('Only zpoints C or F are allowed.')        
        # Assume that the first index of h and f is at the bottom of the domain.
        # To determine the depth, we have to sum from the top
        zf = -cumsum(h[::-1],axis=0)
        f = f[::-1]
        # handle multiple dimensions
        sh = f.shape
        if len(sh)==2:
            fz = empty((len(z), sh[1]))
        elif len(sh)==3:
            fz = empty((len(z), sh[1], sh[2]))
        for j in arange(sh[1]):
            if len(sh)==3:
                for i in arange(sh[2]):
                    fz[:,j,i] = interp(-z, -zf[:,j,i], f[:,j,i])
            elif len(sh)==2:
                fz[:,j] = interp(-z, -zf[:,j], f[:,j])
        return fz                    

    def compute_uflux(self, iter=None):
        """Does the zonal layers calculation at a particular iteration"""
        if iter==None: iter = self.model.default_iter
        u = self.model.rdmds(self.u_fname, iter)
        g = self.model.cgrid_to_ugrid( self._load_g_field(iter) )
        return self._compute_flux(u, g, self.model.hFacS)

    def compute_vflux(self, iter=None):
        """Does the zonal layers calculation at a particular iteration"""
        if iter==None: iter = self.model.default_iter
        v = self.model.rdmds(self.v_fname, iter)
        g = self.model.cgrid_to_vgrid( self._load_g_field(iter) )
        return self._compute_flux(v, g, self.model.hFacS)

    def interp_to_g( self, q, g, iter=None):
        """Interpolate c-grid variable (tracer) to isopycnal (g) coordinates"""
        hq, h = self._compute_flux(q, g, self.model.hFacC)
        return ma.masked_array( hq / h, h==0.), h

    def _compute_flux(self, v, g, hFac):
        """Low-level generic function for computing layer flux, assuming all variables on the same grid"""        
        if isinstance(v, ma.masked_array):
            v = v.filled(0.)
        if isinstance(g, ma.masked_array):
            g = g.filled(0.)
        vflux = empty((self.Ng, self.model.Ny, self.model.Nx))
        hv = empty((self.Ng, self.model.Ny, self.model.Nx))       
        if self.use_fortran:
            import fast_layers
            for j in arange(self.model.Ny):
                for i in arange(self.model.Nx):
                    vflux[:,j,i], hv[:,j,i] = fast_layers.interp_column(
                        self.layers_G, v[:,j,i], g[:,j,i], self.model.dzf, self.model.hFacS[:,j,i],
                        self.map_idx, self.map_fac, self.break_idx )
        else:
            for j in arange(self.model.Ny):
                for i in arange(self.model.Nx):
                    vflux[:,j,i], hv[:,j,i] = self._interp_column(
                        v[:,j,i], g[:,j,i], self.model.hFacS[:,j,i] )
        return vflux, hv

    def _interp_column(self, u, g, hFac):
        """Low-level function for partitioning a field u, defined at points g,
            into layers. The other parameters (grid geometry, g_levs, etc.) are derived from the parent class."""
        gzz = self._value_at_zz(g)
        uzz = self._break_into_zz(u)
        layer_idx = self._assign_to_layers(gzz)
        h = zeros(self.Ng)
        uh = zeros(self.Ng)
        dZZf = self.dZZf * self._break_into_zz(hFac)
        for kk in arange(self.Nzz):
            h[layer_idx[kk]] += dZZf[kk]
            uh[layer_idx[kk]] += dZZf[kk] * uzz[kk]
        return uh, h

    def _value_at_zz(self, g):
        """Low-level function to interpolate field g to fine grid zz points"""
        return self.map_fac*g[self.map_idx] + (1-self.map_fac)*g[self.map_idx+1]

    def _break_into_zz(self, u):
        """Low-level function to partition u onto the fine vertical grid, conserving total u integral."""
        return u[self.break_idx]

    def _assign_to_layers(self, gzz):
        """Low-level function that does the hard work of figuring out which points in g belong in which layers."""
        # use the same hunting algorithm as the fortran code
        # layers_G index (e.g. temperature) goes from cold to warm
        # water column goes from warm (k=1) to cold, so initialize the search with the warmest value
        kg = self.Ng-1
        layer_idx = empty(self.Nzz)
        # remember that the top and bottom values of layers_G are totally ignored
        # maybe this is weird, but that is how LAYERS does it
        for kk in arange(self.Nzz):
            g = gzz[kk] # the value of g in this box
            if g >= self.layers_G[self.Ng-1]:
                # the point is in the hottest bin or hotter
                kg = self.Ng-1
            elif g < self.layers_G[1]:
                # the point is in the coldest bin or colder
                kg = 0
            elif (g >= self.layers_G[kg]) and (g < self.layers_G[kg+1]):
                # already in the right bin
                pass
            elif g >= self.layers_G[kg]:
                while (g >= self.layers_G[kg+1] ):
                    kg += 1
            elif g < self.layers_G[kg+1]:
                while (g < self.layers_G[kg]):
                    kg -= 1
            else:
                raise ValueError('Couldn\'t find a bin in layers_G for g=%g' % g)
            layer_idx[kk] = kg
        return layer_idx
    
    def _load_g_field(self, iter):
        if self.layer_nb==1:
            return self.model.rdmds(self.T_fname, iter)
        elif self.layer_nb==2:
            return self.model.rdmds(self.S_fname, iter)
        else:
            raise ValueError('Only THETA (layer_nb=1) and SALT (layer_nb=2) are supported for now')

        

