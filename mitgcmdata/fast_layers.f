C File layers.f
      subroutine interp_column ( 
     I                           layers_G, Ng,
     I                           u, g, dzf, hfac, Nz,
     I                           map_idx, map_fac, break_idx, Nzz,
     O                           uflux, hu )
      Implicit none
      Integer Ng, Nz, Nzz
      Real*8 layers_G(Ng+1)
      Real*8 u(Nz), g(Nz), dzf(Nz), hfac(Nz)
      Real*8 map_fac(Nzz)
      Integer map_idx(Nzz), break_idx(Nzz)
      Real*8 uflux(Ng), hu(Ng)

Cf2py intent(out) uflux
Cf2py intent(out) hu

C     Local variables
      Integer k, kg
      Real*8 uzz,gzz,dz

C     Initialize output arrays
      DO kg = 1,Ng
       uflux(kg) = 0.
       hu(kg) = 0.
      ENDDO
      
      kg = Ng
      DO k = 1,Nzz
C      break up u evenly across the cell
C      remember fortran indices start at 1!
       uzz = u(break_idx(k)+1)
       dz = hfac(break_idx(k)+1) * dzf(break_idx(k)+1) * Nz / Nzz
C      interpolate g  
       gzz=map_fac(k)*g(map_idx(k)+1)+(1.-map_fac(k))*g(map_idx(k)+2)
C      hunt for the correct bin
       IF (gzz .GE. layers_G(Ng)) THEN
C       the point is in the hottest bin or hotter
        kg = Ng
       ELSE IF (gzz .LT. layers_G(2)) THEN
C       the point is in the coldest bin or colder
        kg = 1
       ELSE IF ((gzz.GE.layers_G(kg)).AND.(gzz.LT.layers_G(kg+1))) THEN
C       already on the right bin -- do nothing
       ELSE IF (gzz .GE. layers_G(kg)) THEN
C       have to hunt for the right bin by getting hotter
        DO WHILE (gzz .GE. layers_G(kg+1))
         kg = kg + 1
        ENDDO
       ELSE IF (gzz .LT. layers_G(kg+1)) THEN
C        have to hunt for the right bin by getting colder
        DO WHILE (gzz .LT. layers_G(kg))
         kg = kg - 1
        ENDDO
       ELSE
C       that should have covered all the options
        STOP 'no bin found in layers_G'
       ENDIF
       hu(kg) = hu(kg) + dz
       uflux(kg) = uflux(kg) + dz * uzz
      ENDDO
      
      END
