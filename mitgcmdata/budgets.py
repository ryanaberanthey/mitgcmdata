import numpy as np
import MITgcmutils.mds as mds
import MITgcmmodel
import warnings

class TracerBudget(object):
    """For calculating T, S, and PTRACER budgets."""
    
    def __init__(self, m, diag_GAD_fname, diag_surf_fname,
                    trname='TH',
                    tottend_name=None,
                    surfflux_name=None,
                    surfflux_fac=None,
                    iters=None, avg_iters=True,
                    use_wmass=True,
                    swflux_name=None,
                    swflux_fname=None,
                    usememmap=False):
                 
        # the parent model object
        assert isinstance(m, MITgcmmodel.ModelInstance)
        self.m = m
        
        # the name of the tracer (used to parse mds files)
        self.trname = trname
        
        if self.trname=='TH':
            self.surfflux_fac = (self.m.heat_capacity_cp * self.m.runit2mass)**-1
            self.surfflux_name = 'TFLUX'
            self.tottend_name = 'TOTTTEND'
        elif self.trname=='SLT':
            self.surfflux_fac = self.m.runit2mass**-1
            self.surfflux_name  = 'SFLUX'
            self.tottend_name = 'TOTSTEND'
        else:
            self.surfflux_fac = 1.
        
        # overwrite the defaults if requested
        if surfflux_fac is not None:
            self.surfflux_fac = surfflux_fac
        if surfflux_name is not None:
            self.surfflux_name = surfflux_name
        if tottend_name is not None:
            self.tottend_name = tottend_name
           
        self.use_wmass=use_wmass
        if self.use_wmass:
            self.wmass_name = 'W%sMASS' % self.trname

        self.use_swflux = (swflux_name is not None)
        self.swflux_name = swflux_name
        self.swflux_fname = swflux_fname

        # the necessary input files
        self.diag_GAD_fname = diag_GAD_fname
        self.diag_surf_fname = diag_surf_fname
        
        # output timesteps
        self.iters = iters
        
        try:
            self.Nt = len(self.iters)
        except TypeError:
            self.Nt = 1

        # whether to time-average multiple outputs
        self.avg_iters = avg_iters & (self.Nt > 1)
        
        self.usememmap = usememmap
        
        self.read_files()
        
    def read_files(self):
        """Read in mds input"""
        
        # gad flux
        gad, i, gad_meta = self.m.rdmds(self.diag_GAD_fname, self.iters,
                                        returnmeta=True,
                                        usememmap=self.usememmap)
                                        
        if self.avg_iters:
            # time always comes first
            gad = gad.mean(axis=0)
            
        gad_vars = ['ADVr','ADVx','ADVy','DFrE','DFxE','DFyE','DFrI','KPPg']
        gad_vars_tr = [(v + '_' + self.trname) for v in gad_vars]
        
        fluxes = dict()
        for v, vtr in zip(gad_vars, gad_vars_tr):
            try:
                recidx = gad_meta['fldlist'].index(vtr)
                fluxes[v] = gad[...,recidx,:,:,:]
            except ValueError:
                if v=='KPPg':
                    warnings.warn(vtr + ' was not found in ' + self.diag_GAD_fname)
                else:
                    raise ValueError(vtr + ' was not found in ' + self.diag_GAD_fname)          
 
        self.fluxes = fluxes
        
        tottend_recidx = gad_meta['fldlist'].index(self.tottend_name)
        self.tottend = gad[...,tottend_recidx,:,:,:] / (24*60*60.)
           
        # surface flux
        surffull, isurf, surf_meta = self.m.rdmds(self.diag_surf_fname, self.iters,
                                        returnmeta=True,
                                        usememmap=self.usememmap)
        if self.avg_iters:
            surffull = surffull.mean(axis=0)
        
                                
        surfflux_recidx = surf_meta['fldlist'].index(self.surfflux_name)        
        
        surf = self.surfflux_fac * surffull[...,surfflux_recidx,:,:] 
        
        if self.use_swflux:
            # have to subtract the sw flux from TFLUX
            if self.swflux_fname is None:
                swflux_meta = surf_meta['fldlist']
                swflux_data = surffull
            else:
                swflux_data, isw, swflux_meta = self.m.rdmds(self.swflux_fname, self.iters,
                                    returnmeta=True,
                                    usememmap=self.usememmap)
            swflux_recidx = swflux_meta['fldlist'].index(self.swflux_name)
            swflux = self.surfflux_fac * swflux_data[...,swflux_recidx,:,:]
            #surf -= swflux
            self.swflux = swflux       
 
        if self.use_wmass:
            wmass_recidx = gad_meta['fldlist'].index(self.wmass_name)
            self.surf_wmass = self.m.rk_sign * gad[...,wmass_recidx,0,:,:]
            #surf += self.surf_wmass
        
        self.surfflux = surf
            
    def net_vertical_flux(self):
        """Calculate the net downward tracer flux from advection and diffusion.
        Returns tot_flux, adv_flux, diff_flux"""
        
        adv_flux = self.fluxes['ADVr'].sum(axis=-1).sum(axis=-1)
        df = self.fluxes['DFrE'] + self.fluxes['DFrI']

        if self.fluxes.has_key('KPPg'):
            df += self.fluxes['KPPg']

        diff_flux = df.sum(axis=-1).sum(axis=-1)

        tot_flux = adv_flux + diff_flux
        
        return tot_flux, adv_flux, diff_flux
    
    def net_meridional_flux(self):
        """Calculate the net meridional tracer flux from advection and diffusion.
        Returns tot_flux, adv_flux, diff_flux"""
        
        adv_flux = self.fluxes['ADVy'].sum(axis=-3).sum(axis=-1)
        diff_flux = self.fluxes['DFyE'].sum(axis=-3).sum(axis=-1)
        tot_flux = adv_flux + diff_flux
        
        return tot_flux, adv_flux, diff_flux        
    
    def adv_flux_convergence(self):
        """Advective flux convergence."""
        return -self.m.flux_divergence(
            self.fluxes['ADVx'], self.fluxes['ADVy'], self.fluxes['ADVr'],
            transports_already_volume_weighted=True
        )

    def diff_flux_convergence(self):
        """Diffusive flux convergence. (Not including KPP)."""
        return -self.m.flux_divergence(
            self.fluxes['DFxE'], self.fluxes['DFyE'],
            self.fluxes['DFrE'] + self.fluxes['DFrI'],
            transports_already_volume_weighted=True
        )

    def vdiff_flux_convergence(self):
        """Vertical diffusive flux convergence. (Not including KPP)."""
        return -self.m.flux_divergence(
            w=(self.fluxes['DFrE'] + self.fluxes['DFrI']),
            transports_already_volume_weighted=True
        )
    
    def hdiff_flux_convergence(self):
        """Horizontal diffusive flux convergence."""
        return -self.m.flux_divergence(
            self.fluxes['DFxE'], self.fluxes['DFyE'],
            transports_already_volume_weighted=True
        )

    def kpp_flux_convergence(self):
        """Flux convergence due to KPP."""
            
        if not self.fluxes.has_key('KPPg'):
            return 0.
        
        kpp_flux = self.fluxes['KPPg']

        #recip_vol = np.where( self.m.hFacC > 0.,
        #       (self.m.rac * self.m.drf * self.m.hFacC)**-1, 0.)
        #return - self.m.delta_k(kpp_flux) * recip_vol
        
        return -self.m.flux_divergence(w=kpp_flux,
                     transports_already_volume_weighted=True)
 
    def sw_flux_convergence(self):
        """Penetrating shortwave flux convergence."""

        if not self.use_swflux:
            return 0.
    
        # create a 3[+]D field of downward shortwave flux
        swfac = swfrac(self.m.rf[:-1])
        if self.avg_iters:
            swdown = self.swflux * swfac
        else:
            swdown = self.swflux[...,np.newaxis,:,:] * swfac
        
        recip_drf = np.where( self.m.hFacC > 0.,
                        (self.m.drf * self.m.hFacC)**-1, 0.)  
        swfull = -self.m.rk_sign * (
                           self.m.delta_k(swdown, mask=True) *
                           recip_drf )
        swsurf = -self.m.rk_sign * (self.swflux * recip_drf[0])

        return swfull, swsurf

    def surf_flux_convergence(self):
        recip_drf = np.where( self.m.hFacC > 0.,
                        (self.m.drf * self.m.hFacC)**-1, 0.)  
        return -self.m.rk_sign * (self.surfflux * recip_drf[0])

    def lin_fs_convergence(self):
        recip_drf = np.where( self.m.hFacC > 0.,
                        (self.m.drf * self.m.hFacC)**-1, 0.)  
        return -self.m.rk_sign * (self.surf_wmass * recip_drf[0])


    def flux_convergence(self):
        """Calculate flux convergence at cell centers.
        Positive makes tracer increase. Should match TOTTEND.
        Return c2d, c3d"""
        
        # 2D (surface) components of convergence
        c2d = {}
        c2d['surfflux'] = self.surf_flux_convergence()
        c2d['linfs'] = self.lin_fs_convergence()

        # 3D components of convergence
        c3d = {}
        c3d['adv'] = self.adv_flux_convergence()
        #c3d['diff'] = self.diff_flux_convergence()
        c3d['hdiff'] = self.hdiff_flux_convergence()
        c3d['vdiff'] = self.vdiff_flux_convergence()        
        c3d['kpp'] = self.kpp_flux_convergence()
       
        if self.use_swflux: 
            sw, swsurf = self.sw_flux_convergence()
            c3d['sw'] = sw
            c2d['surfflux'] -= swsurf
        
        # calculate the total
        tot = np.zeros_like(c3d['adv'])
        for v in c2d.itervalues():
            tot[...,0,:,:] += v
        for v in c3d.itervalues():
            tot += v

        c3d['tot'] = tot

        return c2d, c3d

class WaterMassBudget(object):
    """For calculating T, S, and potential density water mass budgets."""
    
    def __init__(self, m, glevs, extra_mask=None):
        
        self.m = m
        # initialize water mass coordinate (called g)
        self.glevs = glevs
        self.delta_g = drho = np.diff(self.glevs)
        self.Nbins = len(self.glevs)-1
        # rho_idx==i means glevs[i-1] <= glevs < glevs[i]
        
        # this is for masking problem parts of the domain, e.g. open boundary
        self.extra_mask = extra_mask
            
    def calc_transformation_rates(self, g, tb, scalefac=None):
        #assert isinstance(tb, TracerBudget)
        c2d, c3d = tb.flux_convergence()
        all_terms = c2d.values() + c3d.values()
        labels = c2d.keys() + c3d.keys()
        return self.transformation_integral(g, scalefac, *all_terms), labels
        
    def transformation_integral(self, g, scalefac=None, *args):
        """Evauluate d/dG \int_G ( args ) dV."""

        # mask any values of g outside the allowed range
        g_m = np.ma.masked_greater_equal(
                    np.ma.masked_less(g, self.glevs.min()), self.glevs.max())
        if self.extra_mask is not None:
            g_m.mask += self.extra_mask
            
        mask_3d = g_m.mask
        
        idx_3d = np.digitize(g_m.compressed(), self.glevs)-1
        assert idx_3d.min()>=0
        assert idx_3d.max()<=self.Nbins

        # for surface fields
        mask_2d = mask_3d[0]
        idx_2d = np.digitize(g_m[0].compressed(), self.glevs)-1
        
        res = []
        for a in args:
            # figure out if field is 2D or 3D
            # if 2D, assumed to be upper surface
            if a.ndim == 2:
                assert a.shape == (self.m.Ny, self.m.Nx)
                
                volfac = self.m.rac[0] * self.m.drf[0] * self.m.hFacC[0]
                if scalefac is not None:
                    volfac *= scalefac[0]
                mask = mask_2d
                idx = idx_2d
            elif a.ndim == 3:
                assert a.shape == self.m.shape
                volfac = self.m.rac * self.m.drf * self.m.hFacC
                if scalefac is not None:
                    volfac *= scalefac
                mask = mask_3d
                idx = idx_3d
            else:
                raise ValueError("Can't deal with 4D input fields.")
                
            # mask and volume weight the field
            a_msk = np.ma.masked_array(a * volfac, mask)
            
            # volume integral
            a_int = np.bincount(idx, weights=a_msk.compressed(), minlength=self.Nbins)
            assert len(a_int) == self.Nbins
            
            # volume integral divided by tracer contour interval
            res.append(a_int / self.delta_g)
        return np.array(res)
        
       
    
    
def swfrac(z, fact=1., jwtype=2):
    """Clone of MITgcm routine for computing sw flux penetration.
    z: depth of output levels"""
    
    rfac = [0.58 , 0.62, 0.67, 0.77, 0.78]
    a1 = [0.35 , 0.6  , 1.0  , 1.5  , 1.4]
    a2 = [23.0 , 20.0 , 17.0 , 14.0 , 7.9 ]
    
    facz = fact * z
    j = jwtype-1
    swdk = np.ma.masked_array(
        rfac[j]     * np.exp( facz / a1[j]) +
        (1-rfac[j]) * np.exp( facz / a2[j]),
        facz < -200
    ).filled(0.)

    return swdk

        
        
