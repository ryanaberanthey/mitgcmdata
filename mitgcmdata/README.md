# MITgcmdata - A Diagnostic Package for MITgcm

This represents my first attempt to make a python package for analyzing the output of MITgcm simulations. It is potentially usefuly, but also contains lots of quirks. I am writing a completely new version from scratch that will adhere more closely to the model numerics and conventions. In the meantime, I decided to publish this anyway, in case someone finds it useful.

It is also not really properly structured as a python pacakge. It is really just a single module.

## Basic Usage

The package is based around a class called MITgcmmodel.ModelInstance. This class contains many useful methods. A basic example is

	from MITgcmdata import MITgcmmodel
	# initalize the model instance
	m = MITgcmmodel.ModelInstance(
                output_dir='/path/to/output/files',
		grid_dir='/path/to/grid/files/',
		default_iter=112233)
	# load a variable
	# (this looks for THETA.0000112233.data in the output_dir)
	theta = m.rdmds('THETA')

There are lots of operators for derivatives, integrals etc. I will try to upload more examples at some point.


