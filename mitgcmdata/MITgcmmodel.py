"""Python module for working with MITgcm output"""

import numpy as np
import os.path
import MITgcmutils.mds as mds
import warnings

class ModelInstance:
    """Contains a general description of a particular GCM grid and configuration"""

    def __init__(self, output_dir, grid_dir=None, datafile=None, 
                 grid_type='cartesian', useMNC=False, default_iter=0, useMask=True,
                 eosType='LINEAR', g=9.8, tAlpha=2e-4, sBeta=0.,
                 rk_sign=-1,
                 heat_capacity_cp=3.994e3,
                 runit2mass=1.035e3,
                 ):

        self.output_dir = output_dir
        if grid_dir==None:
            self.grid_dir = output_dir
        else:
            self.grid_dir = grid_dir

        self.default_iter=default_iter

        if datafile==None:
            self.datafile = os.path.join(output_dir,'data')
        else:
            self.datafile = datafile

        self.eosType = eosType
        self.g = g
        self.tAlpha = tAlpha
        self.sBeta = sBeta

        self.rk_sign = rk_sign
        self.heat_capacity_cp = heat_capacity_cp
        self.runit2mass = runit2mass

        self.useMask = useMask
        self.grid_type = grid_type

        self.load_grid()
        print( '%3d %3d %3d' % (self.Nx,self.Ny,self.Nz) )

    def load_grid(self):
        # horizontal
        self.xc = mds.rdmds(os.path.join(self.grid_dir, 'XC'))[np.newaxis,:,:]
        self.xg = mds.rdmds(os.path.join(self.grid_dir, 'XG'))[np.newaxis,:,:]
        self.yc = mds.rdmds(os.path.join(self.grid_dir, 'YC'))[np.newaxis,:,:]
        self.yg = mds.rdmds(os.path.join(self.grid_dir, 'YG'))[np.newaxis,:,:]
        self.dyc = mds.rdmds(os.path.join(self.grid_dir, 'DYC'))[np.newaxis,:,:]
        self.dyg = mds.rdmds(os.path.join(self.grid_dir, 'DYG'))[np.newaxis,:,:]
        self.dxc = mds.rdmds(os.path.join(self.grid_dir, 'DXC'))[np.newaxis,:,:]
        self.dxg = mds.rdmds(os.path.join(self.grid_dir, 'DXG'))[np.newaxis,:,:]
        self.rac = mds.rdmds(os.path.join(self.grid_dir, 'RAC'))[np.newaxis,:,:]
        
        self.rc = mds.rdmds(os.path.join(self.grid_dir, 'RC'))
        self.rf = mds.rdmds(os.path.join(self.grid_dir, 'RF'))
        self.drc = mds.rdmds(os.path.join(self.grid_dir, 'DRC'))
        self.drf = mds.rdmds(os.path.join(self.grid_dir, 'DRF'))
        self.zc = self.rc.squeeze()
        self.zf = self.rc.squeeze()
        self.depth = mds.rdmds( os.path.join(self.grid_dir, 'Depth') )

        # masks
        self.hFacC = mds.rdmds( os.path.join(self.grid_dir, 'hFacC') )
        self.hFacS = mds.rdmds( os.path.join(self.grid_dir, 'hFacS') )
        self.hFacW = mds.rdmds( os.path.join(self.grid_dir, 'hFacW') )
        self.mask = (self.hFacC==0.)
        
        # for derivatives
        self.recip_volC = np.where( self.hFacC > 0.,
               (self.rac * self.drf * self.hFacC)**-1, 0.)

        try:
            self.Nr, self.Ny, self.Nx = self.hFacC.shape
        except ValueError:
           self.Ny, self.Nx = self.hFacC.shape
           self.Nr = 1
        self.Nz = self.Nr
        
        # new MITgcm code makes dzc length Nz+1
        # this breaks our code
        if len(self.drc)==(self.Nz+1):
            self.drc = self.drc[:-1]

        self.shape = (self.Nz, self.Ny, self.Nx)
        self._setup_bottom_idx()
        # for integrals

    def mask_field(self, T):
        return np.ma.masked_array(T, self.mask)

    def rdmds(self, varname, itrs=None, useMask=False, **kwargs):
        """Shortcut to mds.rdmds that includes masking capability"""
        if itrs==None:
            itrs = self.default_iter

        out = mds.rdmds( os.path.join(self.output_dir, varname), itrs, **kwargs )

        if useMask:
            out = self.mask_field(out)

        return out

    # bottom stuff
    def _setup_bottom_idx(self):
        mask = self.mask[np.r_[:self.Nz,self.Nz-1]]
        mask[-1] = True
        isbot = ~mask[:self.Nz] & mask[1:]
        # deal with the surface
        isbot[0] = mask[0]
        isbot_mask = np.zeros_like(isbot)
        isbot_mask[0] = mask[0]
        # use fortran byte order to do ravel
        self._bot_idx = np.where(np.ravel(isbot, order='F'))[0]
        self._bot_idx_mask = np.ravel(isbot_mask, order='F')[self._bot_idx]
        
    def value_at_bottom(self, q, point='C'):
        qb = ma.masked_array( ravel(q, order='F')[self._bot_idx], self._bot_idx_mask)
        qb.shape = (self.Nx, self.Ny)
        return copy(transpose(qb))
        
 
    def calc_surface_flux(self, Tsurf, Trelax=None, tau_surf_relax=2592000.):
        """Calculate surface relaxation flux"""
        if Trelax is None:
            Trelax = linspace(0,8.,self.Ny)[:,np.newaxis]
        return -tau_surf_relax**-1 * (
            Tsurf - Trelax
        )*self.rho0*self.cp*self.drf[0]
        
    # helper function for calculation momentum flux divergence
    def mom_flux_divergence(self, uu, vv, uv):
        """Calculate u and v momentum tendency due to advection.
        Assumes uu and vv are on the usual grid points and vv
        is on the cell corner
        """
        u_transient_flux_divergence = (
            # centered difference for u
            (self.shift_i(uu,-1) - self.shift_i(uu,1))/(2*self.dxg) +
            # first order for v
            (self.shift_j(uv, -1))
        )
    
        v_transient_flux_divergence = (
            # first order for u
            (self.shift_j(uv, -1)) +
            # centered difference for v
            (self.shift_j(vv,-1) - self.shift_j(vv,1))/(2*self.dyg) 
        )
        return u_transient_flux_divergence, v_transient_flux_divergence

    # stuff for calculating fluxes
    def flux_divergence(self,
            u=None, v=None, w=None, tr=None,
            transports_already_volume_weighted=False,
            output_volume_weighted=False):
        """Calculate the divergence of advective fluxes
        into MITgcm grid boxes for the specified tracer tr
        and transport velocities u, v, w."""
        
        if u is None:
            ui = 0.
        else:
            ui = u
        if v is None:
            vi = 0.
        else:
            vi = v
        if w is None:
            wi = 0.
        else:
            wi = w
        
        if not transports_already_volume_weighted:
            # have to volume weight transports
            U = ui * self.dyg * self.drf * self.hFacW
            V = vi * self.dxg * self.drf * self.hFacS
            W = wi * self.rac
        else:
            U, V, W = ui, vi, wi
        
        if tr is not None:
            Uflux = U * self.shift_i(tr)
            Vflux = V * self.shift_j(tr)
            Wflux = W * self.shift_k(tr)
        else:
            Uflux, Vflux, Wflux = U, V, W
        
        div = 0.
        if u is not None:
            div += self.delta_i(Uflux)
        if v is not None:
            div += self.delta_j(Vflux)
        if w is not None:
            div += self.delta_k(Wflux)
        
        if output_volume_weighted:
            return div
        else:
            return div * self.recip_volC
        
    def shift_i(self, c, d=1):
        """Average field d gridpoints to the east"""
        return 0.5 * (c + np.roll(c, d, axis=-1))
        
    def shift_j(self, c, d=1):
        """Average field d gridpoints to the south"""
        return 0.5 * (c + np.roll(c, d, axis=-2))

    def shift_k(self, c, d=1):
        if d==1:
            # shift up
            return 0.5 * (c + 
                    np.concatenate(
                        (c[...,0,:,:][...,np.newaxis,:,:],
                         c[...,:-1,:,:]),
                        axis=-3)
                    )
        elif d==-1:
            # shift down
            return 0.5 * (c + 
                    np.concatenate(
                        (c[...,1:,:,:],
                         c[...,:-1,:,:][...,np.newaxis,:,:]),
                        axis=-3)
                    )
        else:
            raise ValueError('Only d = +/- 1 is supported.')
                    
    def delta_i(self, c, d=-1):
        """Difference field one gridpoint to the east."""
        return np.roll(c, d, axis=-1) - c
    
    def delta_j(self, c, d=-1):
        """Difference field one gridpoint to the south."""
        return np.roll(c, d, axis=-2) - c
        
    def delta_k(self, c, d=-1, mask=False):
        """Difference field one gridpoint in depth"""
        if mask:
            cm = c*(~self.mask)
        else:
            cm = c
        # local array shape
        c_zero = np.zeros_like(c[...,0,:,:])[...,np.newaxis,:,:]
        if d==-1:
            return self.rk_sign * (
                np.concatenate((cm[...,1:,:,:], c_zero), axis=-3) - cm
            )
        elif d==1:
            return self.rk_sign * (
                np.concatenate((c_zero, cm[...,:-1,:,:]), axis=-3) - cm
            )
        elif d==0:
            # centered difference
            out = 0.5*( self.delta_k(cm,1) + self.delta_k(cm,-1) )
            out[...,0,:,:] = self.rk_sign * (cm[...,1,:,:] - cm[...,0,:,:])
            out[...,-1,:,:] = self.rk_sign * (cm[...,-1,:,:] - cm[...,-2,:,:])
            return out
        else:
            raise ValueError('Only d = -1,0,+1 is supported.')
                                    
    def integrate(self, c, axes=(-3,-2,-1), output_average=False,
                  klims=None, jlims=None, ilims=None):
        """Volume integrate a c-grid variable"""
        
        if c.ndim < 3:
            raise ValueError('c must have at least dimension 3')
        shape_space = c.shape[-3:]
        #assert shape_space = self.shape
            
        if klims is None:
            klims = (0, self.Nr)
        if jlims is None:
            jlims = (0, self.Ny)
        if ilims is None:
            ilims = (0, self.Nx)
        
        cint = c
        
        print('cint.shape = ', cint.shape)
        
        hfac = 1.
        # vertical axis
        if -3 in axes:
            cint = (cint[...,klims[0]:klims[1],:,:] * 
                     self.drf[klims[0]:klims[1]] *
                     self.hFacC[klims[0]:klims[1]]
                    ).sum(axis=-3)[...,np.newaxis,:,:]
            if output_average:
                cint /= (self.drf[klims[0]:klims[1]] +
                         self.hFacC[klims[0]:klims[1]]
                         ).sum(axis=-3)[np.newaxis,:,:]
        else:                 
            # if we averaged in the vertical, we already took care of the
            # issue of masked cells by multiplying by hFaC
            # otherwise we need to account for hFaC when averaging
            if output_average and shape_space==self.shape:
                hfac = self.hFacC[
                        klims[0]:klims[1],
                        jlims[0]:jlims[1],
                        ilims[0]:ilims[1]]
        
        # if doing both x and y, can use rac
        if (-2 in axes) and (-1 in axes):
            print("Integrating in x and y")
            print(' tmp.shape = ', cint[...,:,jlims[0]:jlims[1],ilims[0]:ilims[1]].shape)
            cint = ( cint[...,:,jlims[0]:jlims[1],ilims[0]:ilims[1]] * 
                     self.rac[:,jlims[0]:jlims[1],ilims[0]:ilims[1]] *
                     hfac
                    ).sum(axis=-1).sum(axis=-1)[...,:,np.newaxis,np.newaxis]
            print("Now cint.shape = ", cint.shape)
            if output_average:
                cint /= (self.rac[:,jlims[0]:jlims[1],ilims[0]:ilims[1]] *
                         hfac
                         ).sum(axis=-1).sum(axis=-1)[:,np.newaxis,np.newaxis]
        elif (-2 in axes):
            cint = (cint[...,:,jlims[0]:jlims[1],:]*self.dyg[:,jlims[0]:jlims[1],:] *
                    hfac
                    ).sum(axis=-2)[...,:,np.newaxis,:]
            if output_average:
                cint /= (self.dyg[:,jlims[0]:jlims[1]] * hfac
                         ).sum(axis=-2)[:,np.newaxis,:]
        elif (-1 in axes):
            cint = (cint[...,:,:,ilims[0]:ilims[1]]*self.dxg[:,:,ilims[0]:ilims[1]] *
                    hfac
                    ).sum(axis=-1)[...,:,:,np.newaxis]
            if output_average:
                cint /= (self.dxg[:,:,ilims[0]:ilims[1]] * hfac
                         ).sum(axis=-1)[:,:,np.newaxis]
        
        return np.ma.masked_invalid(cint)

        
