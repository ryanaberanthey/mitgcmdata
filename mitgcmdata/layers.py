import numpy as np
import MITgcmutils.mds as mds

class LayersAnalyzer(object):
    
    def __init__(self, m, layers_name='1RHO'):

        self.m = m
        self.layers_name = layers_name
        self.layers_num = layers_name[0]
        self.layers_type = layers_name[1:]
        
        if not self.layers_type in ['TH', 'SA', 'RHO']:
            raise ValueError('layers type must be TH, SA, or RHO')
        
        if self.layers_type == 'RHO':
            self.ksign = -1
        else:
            self.ksign = 1
        
        # layer bounds
        self.layers_bounds = mds.rdmds(
                    self.m.output_dir + '/layers' + self.layers_name
            ).squeeze()[::self.ksign]

        self.Nlayers = len(self.layers_bounds)-1
            
        # w-cell bounds
        self.layers_bounds_w = 0.5 * (
                self.layers_bounds[1:] + self.layers_bounds[:-1])
                
        # top points (where w is computed)
        self.layers_top = self.layers_bounds[:-1]
        
    def volume_trend(self, Hc, dt):
        """Calcualte the volume trend at each point, given the appropriate
        layers diagnostic output.
          Hc - the LaHc snapshots
          dt - the time spacing between snapshots (seconds)
        """
        
        # need at least two different time snapshots
        assert Hc.shape[1] == self.Nlayers
        assert Hc.shape[0] > 1
        
        vol = np.cumsum( Hc[:,::self.ksign] * self.m.rac, axis=1)
        dvol_dt = np.diff(vol, axis=0) / dt
        return dvol_dt[:,:-1].squeeze()
        
    def diapycnal_velocity(self, d):
        """Calculate diapycnal velocity, given the appropriate
        layers output."""
        
        #Ts, Th, Tr, Tha, Tra, Ss, Sh, Sr, Sha, Sra = m.rdmds('DiagLAYERS-diapycnal')[:,::ksign]
        
        wflux = d[:,::self.ksign] * self.m.rac
        return wflux
        
    def advective_flux_divergence(self, uh, vh):
        """Calculate the advective diapycnal velocity resulting
        from advective flux divergence."""
        uflux = uh[::self.ksign] * self.m.dyg
        vflux = vh[::self.ksign] * self.m.dxg
        div_uflux = (np.roll(uflux,-1,axis=-1)-uflux).cumsum(axis=0)
        div_vflux = (np.roll(vflux,-1,axis=-2)-vflux).cumsum(axis=0)

        # don't include top point because we don't have a diapycnal velocity there
        wflux = -(div_uflux[:-1] + div_vflux[:-1])
        return wflux
    
    def calculate_moc(self, vh):
        """Calculate overturning streamfunction given vh"""
        psi = self.ksign * self.m.integrate(
                vh[::self.ksign], axes=(-1,)).squeeze().cumsum(axis=0)
        return psi
    
    def interp_layers_to_z(self, field, hc, zlevs, wpoint=True):
        """Interpolate a layers-coordinate field (field), with
        layer thicknesses given by hc, to the points in zlevs.
        If wpoint=True, assume that field is given at the
        tops of the layer cells."""
        
        from scipy.interpolate import interp1d
        
        old_shape = field.shape

        assert hc.ndim == 2
        assert old_shape[-1] == (hc.shape[-1]+1)
                
        ztop = np.cumsum(hc[...,:-1,:], axis=-2)
        
        Nz = len(zlevs)
        new_shape = list(old_shape)
        new_shape[-2] = Nz
        field_z = np.zeros(new_shape, dtype=field.dtype)
        
        # now have to do a stupid slow loop
        Ny = old_shape[-1]
        for j in xrange(Ny):
            field_z[...,:,j] = interp1d(
                ztop[:,j],
                field[...,:,j],
                axis=-1,
                copy=False,
                assume_sorted=True,
                bounds_error=False,                    
            )(zlevs)
        
        return np.ma.masked_invalid(field_z)
        
        
        
